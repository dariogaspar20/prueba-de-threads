package com.example.mylibrary;

import android.util.Log;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.content.ContentValues.TAG;

public class MyClase {
    private static String TAG  =  "Unity:";

    public static void Mimetodo(){
        Log.d(TAG, "Mimetodo: Hola desde android");
    }


    public  static  int devolver() throws InterruptedException {
         final Claseprueab aux = new Claseprueab();

         Thread df= new Thread(new Runnable() {
             @Override
             public void run() {
                 Log.d(TAG, "run: Comenzo el hilo");
                 try {
                     Thread.sleep(10000);
                 } catch (InterruptedException e) {
                     e.printStackTrace();
                 }
                 Log.d(TAG, "run: Termina el  hilo");
                 aux.setValor(543);
             }
         });
         df.start();

        Log.d(TAG, "devolver: Despues de iniciar el hilo");
        //while(df.isAlive()){
            //Log.d(TAG, "devolver: Entro al if porque el  hilo es: "+ df.isAlive());
          //  Thread.yield();
        //}
        //Log.d(TAG, "devolver: Salio del if");
        df.join();
        Log.d(TAG, "devolver: Despues de termina el hilo");
        return aux.get();
    }


    public static void metodo1() throws InterruptedException {
        Log.d(TAG, "metodo1: Entro al metodo1");
        int aux = devolver();
        Log.d(TAG, "metodo1: TErmino al metodo1");
    }

    public static class Claseprueab{
        int valor;

        public Claseprueab(){
            valor = 3;
        }

        public int get()
        {
            return valor;
        }

        public void setValor(int gg){
            valor = gg;
        }
    }
}
